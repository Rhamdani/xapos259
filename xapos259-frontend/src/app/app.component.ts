import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './model/user';
import { AuthService } from './services/au.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
 
  user: User = new User();

  constructor(private authService: AuthService, private route: Router ) {}

  ngOnInit(): void {
    const tokens = this.authService.loggedIn();
    if (tokens) {
      this.route.navigate(['/home']);    
    } 
  }
  title = 'xapos259-frontend';
}
