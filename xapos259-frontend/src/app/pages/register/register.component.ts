import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Register } from 'src/app/model/register';
import { AuthService } from 'src/app/services/au.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public register: Register = new Register();

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  authRegister() {
    this.authService.register(this.register).subscribe(
      (data: any) => {
        console.log(data);
        this.router.navigate(['login']);
      },
      (err) => {
        console.log(err.error);
      },
      () => console.log('Register Berhasil')
    )
  }

  move_to_login() {
    this.router.navigate(['login']);
  }
}
