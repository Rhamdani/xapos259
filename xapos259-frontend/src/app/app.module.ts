import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule } from '@angular/forms';
import { appRoutingModule } from './routing/app-routing.module';
import { AuthService } from './services/au.service';
import { RegisterComponent } from './pages/register/register.component';
import { NavComponent } from './component/nav/nav.component';
import { CategoryComponent } from './pages/category/category.component';
import { HomeComponent } from './pages/home/home.component';
import { VariantComponent } from './pages/variant/variant.component';
import { AuthGuard } from './guards/auth.guard';
import { ProductComponent } from './pages/product/product.component';

@NgModule({
  declarations: [AppComponent, LoginComponent, RegisterComponent, NavComponent, CategoryComponent, HomeComponent, VariantComponent, ProductComponent],
  imports: [BrowserModule, HttpClientModule, FormsModule, appRoutingModule],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule { }
