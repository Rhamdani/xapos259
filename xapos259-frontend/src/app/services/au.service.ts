import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../base/config';
import { Register } from '../model/register';

import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public login(user: User) {
    return this.http.post(Config.url + 'api/auth/signin', user);
  }
  public register(register: Register) {
    return this.http.post(Config.url + 'api/auth/signup', register);
  }

  public loggedIn() {
    return !!localStorage.getItem('token');
  }
}
