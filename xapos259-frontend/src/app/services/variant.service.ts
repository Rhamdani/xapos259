import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../base/config';
import { Variant } from '../model/variant';


@Injectable({
  providedIn: 'root'
})
export class VariantService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    })
  }

  public getVariant(): Observable<Variant[]> {
    return this.http.get<Variant[]> (
      `${Config.url}api/variant`,
      this.httpOptions
    )
  }

  public addVariant(variant: Variant): Observable<Variant> {
    return this.http.post<Variant>
      (Config.url + 'api/variant',
      variant, {
        ...this.httpOptions,responseType: 'text' as 'json',
      }
    )
  }

  public editVariant(variant: Variant): Observable<Variant> {
    return this.http.put<Variant>
      (Config.url + 'api/variant',
      variant, {
        ...this.httpOptions,responseType: 'text' as 'json',
      }
    )
  }

  public deleteVariant(id : number): Observable<void> {
    return this.http.delete<void>
      (Config.url + `api/variant/${id}`, {
        ...this.httpOptions,responseType: 'text' as 'json',
      }
    )
  }
}
