import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  token: string = '';

  constructor(private route: Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')!!;
  }

  public onLogout() {
    localStorage.removeItem('token');
    this.route.navigate(['/login']);
  }
}
