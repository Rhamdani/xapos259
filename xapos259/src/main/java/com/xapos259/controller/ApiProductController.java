package com.xapos259.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos259.model.Product;
import com.xapos259.repository.ProductRepository;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/")
public class ApiProductController {

	@Autowired
	private ProductRepository productRepository;
	
	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllproduct() {
		try {
			List<Product> product = this.productRepository.findAll();
			return new ResponseEntity<>(product, HttpStatus.OK);
		} catch (Exception err) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("product/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<List<Product>> getproductById(@PathVariable("id") Long id) {
		try {
			Optional<Product> product = this.productRepository.findById(id);
			if (product.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(product, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("product")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> saveproduct(@RequestBody Product product) {
		Product productData = this.productRepository.save(product);
		if (productData.equals(product)) {
			return new ResponseEntity<>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("product")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> updateproduct(@RequestBody Product product) {
		Long id = product.getId();
		Optional<Product> productData = this.productRepository.findById(id);
		if (productData.isPresent()) {
			product.setId(id);
			this.productRepository.save(product);
			return new ResponseEntity<>("Update Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("product/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> deleteproduct(@PathVariable("id") Long id) {
		this.productRepository.deleteById(id);
		return new ResponseEntity<>("Delete Success", HttpStatus.OK);
	}
}
