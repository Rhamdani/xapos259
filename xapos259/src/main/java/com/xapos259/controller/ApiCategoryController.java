package com.xapos259.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos259.model.Category;
import com.xapos259.repository.CategoryRepository;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/")
public class ApiCategoryController {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping("category")
	public ResponseEntity<List<Category>> getAllCategory() {
		try {
			List<Category> category = this.categoryRepository.findAll();
			return new ResponseEntity<>(category, HttpStatus.OK);
		} catch (Exception err) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("category/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<List<Category>> getCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<Category> category = this.categoryRepository.findById(id);
			if (category.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(category, HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	
	@PostMapping("category")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> saveCategory(@RequestBody Category category) {
		Category categoryData = this.categoryRepository.save(category);
		if (categoryData.equals(category)) {
			return new ResponseEntity<>("Save Success", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("category")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> updateCategory(@RequestBody Category category) {
		Long id = category.getId();
		Optional<Category> categoryData = this.categoryRepository.findById(id);
		if (categoryData.isPresent()) {
			category.setId(id);
			this.categoryRepository.save(category);
			return new ResponseEntity<>("Update Success", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("category/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id") Long id) {
		this.categoryRepository.deleteById(id);
		return new ResponseEntity<>("Delete Succes", HttpStatus.OK);
	}		
}
