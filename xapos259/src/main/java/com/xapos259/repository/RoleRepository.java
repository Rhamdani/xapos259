package com.xapos259.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos259.model.ERole;
import com.xapos259.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
}
